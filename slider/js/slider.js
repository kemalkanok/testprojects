﻿$.fn.slide = function (data) {
	if(data === undefined)
	{
		data = 
		{
			width:300,//px
    		time:2,//sn
    		direction:1//1 for left , 0 for right
		}
	}
    var obj = 
    {
        width: data.width,
        time: data.time,
        direction:data.direction,
    	start:0,
    	slide:function  (direction) {
    		el.animate({
    			"left":-obj.width*obj.start + "px"},
    			"slow");
    		if(direction == 1)
    		{
    			obj.start =  (obj.start + 1) % slides.length ;
    		}
    		else
    		{
    			if(obj.start == 0)
    			{
    				obj.start = slides.length-1;
    			}
    			else
    			{
    				obj.start--;
    			}
    		}
    		window.setTimeout(function() { obj.slide(direction) }, 1000*obj.time)
    	}
    };
    var el = this.eq(0);
    var slides = el.children();
    el.css("width", obj.width * slides.length);
    obj.slide(obj.direction);
};

$(document).ready(function () {
    $(".slider").slide({
        width: 300,//px
        time: 2,//sn
        direction: 0//1 for left , 0 for right
    });

    $(".slider").eq(1).slide({
        width: 300,//px
        time: 2,//sn
        direction: 0//1 for left , 0 for right
    });

});